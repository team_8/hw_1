#include <jni.h>
#include <string>
#include <vector>
#include <cstring>
int priority(char);
double operate(double, double,char);

extern "C" JNIEXPORT jstring JNICALL
//기본적으로 결과창에 띄워 줄 메시지.
Java_com_example_hw_11_MainActivity_stringFromJNI(JNIEnv *env, jobject /* this */)
{
    std::string hello = "This is result";
    return env->NewStringUTF(hello.c_str());
}
extern "C" JNIEXPORT jstring JNICALL
//실질적으로 계산을 담당하는 함수
Java_com_example_hw_11_MainActivity_calculateFromJNI(JNIEnv *env, jobject, jstring text)
{
    // TODO: implement calculateFromJNI()
    std::vector<std::string> vector_op;
    std::vector<double> vector_num;
    std::vector<std::string> vector_postfix;
    std::vector<std::string> vec;
    double result,ca;
    long long size;
    std::string result_text = "";
    const char *temp = env->GetStringUTFChars(text, 0);

    char* tok1 = strtok((char*)temp," ");

    result = 0;
    while(tok1!=NULL){
        //후위연산식으로 바꾸는 과정
        std::string cal_text = tok1;
        int count = 0;
        double t_n = atof(cal_text.c_str());
        if((t_n != 0) || (t_n == 0 && cal_text.at(0) == '0')){
            //입력한 문자가 0을 포함한 수 일경우
            vector_postfix.push_back(cal_text);
        }
        else{
            if(vector_op.empty()){
                vector_op.push_back(cal_text);
            } else if(priority(vector_op.back().at(0)) >= priority(cal_text.at(0))){
                vector_postfix.push_back(vector_op.back());
                vector_op.pop_back();
                vector_op.push_back(cal_text);
            } else{
                vector_op.push_back(cal_text);
            }
        }
        vec.push_back(cal_text);
        tok1 = strtok(NULL," ");
    }
    while(!vector_op.empty()) {
        vector_postfix.push_back(vector_op.back());
        vector_op.pop_back();
    }

    for(int i = 0; i<vector_postfix.size(); i++)
    {
        double t_n = atof(vector_postfix.at(i).c_str());
        //숫자일경우
        if((t_n != 0) || (t_n == 0 && vector_postfix.at(i).at(0) == '0')){
            vector_num.push_back(t_n);
        }
        //문자일경우
        else{
            if(vector_num.size() < 2) {
                result_text = "올바른 식이 아닙니다.";
                return env->NewStringUTF(result_text.c_str());
            }
            double a,b;
            a = vector_num.back();
            vector_num.pop_back();
            b = vector_num.back();
            vector_num.pop_back();
            vector_num.push_back(operate(a,b,vector_postfix.at(i).at(0)));
        }
    }
    result = vector_num.front();

    //띄어쓰기를 기준으로 문자열을 나눔.(연산기호의 앞 뒤로 띄어쓰기가 있음)
    /*

    //첫번째 문자가 연산기호일 경우 에러
    if((result = atof(vec[0].c_str())) == 0 && vec[0].at(0) != '0'){
        result_text = "올바른 식이 아닙니다.";
        return env->NewStringUTF(result_text.c_str());
    }
    //마지막 문자가 연산기호일 경우 에러
    if(atof(vec[size-1].c_str()) == 0 && vec[size-1].at(0) != '0'){
        result_text = "올바른 식이 아닙니다.";
        return env->NewStringUTF(result_text.c_str());
    }

    for(int i = 1; i<vec.size(); i+=2)
    {
        //짝수 번째에는 숫자만, 홀수 번째에는 기호만 들어와야 함.
        //숫자가 아니면 에러(짝수번째)
        if((ca = atof(vec[i+1].c_str())) == 0 && vec[i+1].at(0) != '0'){
            result_text = "올바른 식이 아닙니다.";
            return env->NewStringUTF(result_text.c_str());
        }
        switch (vec[i].at(0)) {
            case '+' :
                result += ca;
                break;
            case '-' :
                result -= ca;
                break;
            case '*' :
                result *= ca;
                break;
            case '/' :
                if(ca == 0){
                    result_text = "올바른 식이 아닙니다.";
                    return env->NewStringUTF(result_text.c_str());
                }
                result /= ca;
                break;
            case '%' :
                //소수일경우 소수점 이하를 버리고 계산.
                if((long long)ca == 0){
                    result_text = "올바른 식이 아닙니다.";
                    return env->NewStringUTF(result_text.c_str());
                }
                result = (long long)result % (long long)ca;
                break;
            case '^' :
                if (ca == 0) {
                    result = 1;
                } else {
                    long long b;
                    double save = result;
                    double t;
                    result = 1;
                    while(ca >= 1){
                        b = 2;
                        t = save;
                        while(b <= ca){
                            t *= t;
                            b = b<<1;
                        }
                        result *= t;
                        ca-= b>>1;
                    }
                }
                break;
            default:
                result = 0;
                break;
        }
    }
     */
    if(result - (long long)result == 0){
        result_text = std::to_string((long long)result);
    } else {
        char * tta = new char[std::to_string(result).length()];
        sprintf(tta,"%g",result);
        result_text = tta;
    }

    return env->NewStringUTF(result_text.c_str());
}

int priority(char op)
{
    switch(op){
        case '+' :
        case '-' :
            return 1;
        case '*' :
        case '/' :
            return 2;
        case '(' :
        case ')' :
            return 3;
        default :
            return -1;
    }
}

double operate(double a, double b, char op)
{
    switch(op){
        case '+' :
            return a+b;
        case '-' :
            return a-b;
        case '*' :
            return a*b;
        case '/' :
            return a/b;
    }
}

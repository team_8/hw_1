package com.example.hw_1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private TextView tv;
    private TextView calcv;
    private int parencount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parencount = 0;
        tv = findViewById(R.id.t_result);
        calcv = findViewById(R.id.t_calculate);
        tv.setText(stringFromJNI());
        calcv.setText("");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ResourceType")
    //연산기호를 입력했을때 호출되는 함수.
    public void clickChar(View view) {
        // Do something in response to button
        CharSequence temp = calcv.getText();
        //연산자가 2번이상 연속될 경우 또는 소수점 이후 바로 연산자를 입력할 경우 입력되지 않게함.
        if(temp.length() == 0) return;
        if(temp.charAt(temp.length()-1) == ' ' || temp.charAt(temp.length()-1) == '.') return;
        Button b = (Button)view;
        temp = temp + " "+ b.getText().toString() + " ";

        calcv.setText(temp);
    }

    public void clickMinus(View view) {
        // Do something in response to button
        CharSequence temp = calcv.getText();
        //연산자가 2번이상 연속될 경우 또는 소수점 이후 바로 연산자를 입력할 경우 입력되지 않게함.
        if(temp.length() > 0) {
            if(temp.charAt(temp.length() - 1) == '.') return;
        }
        if(temp.length() > 1){
            if(temp.charAt(temp.length() - 2) == '-') return;
        }
        Button b = (Button)view;
        temp = temp + " "+ b.getText().toString() + " ";

        calcv.setText(temp);
    }
    //숫자를 입력했을 때 호출되는 함수
    public void clickNum(View view) {
        // Do something in response to button
        CharSequence temp = calcv.getText();
        // ) 다음에는 숫자를 입력할 수 없음.
        if(temp.length() != 0 && temp.charAt(temp.length()-1) == ')') return;
        Button b = (Button)view;
        temp = temp + b.getText().toString();

        calcv.setText(temp);
    }
    //소수점을 입력했을 때 호출되는 함수
    public void clickDot(View view) {
        // Do something in response to button
        CharSequence temp = calcv.getText();
        String str = temp.toString();
        String result = str.substring(str.lastIndexOf(" ")+1);
        //소수점을 입력하려는 숫자에 이미 소수점이 찍혀있다면 소수점을 입력하지않음.
        //닫는 괄호이후에는 소수점을 입력하지 않음.
        for(int i = 0; i<result.length();i++){
            if(result.charAt(i) == '.' || result.charAt(i) == ')'){
                return;
            }
        }
        Button b = (Button)view;
        temp = temp + b.getText().toString();

        calcv.setText(temp);
    }

    public void clickLparen(View view) {
        // Do something in response to button

        CharSequence temp = calcv.getText();
        //여는 괄호 입력 전에 연산자가 있어거나 아무것도 없어야 함.
        if(temp.length() != 0 && temp.charAt(temp.length()-1) != ' ') return;

        Button b = (Button)view;
        temp = temp + b.getText().toString() + " ";
        parencount++;

        calcv.setText(temp);
    }

    public void clickRparen(View view) {
        // Do something in response to button
        //괄호의 짝이 안맞으면 입력받지 않음.
        if(parencount == 0) return;
        CharSequence temp = calcv.getText();
        //닫는 괄호 입력 전에 연산자나 소수점이 있으면 안됨.
        if(temp.length() == 0) return;
        if(temp.charAt(temp.length()-1) == ' ' || temp.charAt(temp.length()-1) == '.') return;

        Button b = (Button)view;
        temp = temp + " " + b.getText().toString();
        parencount--;
        calcv.setText(temp);
    }

    //캔슬 버튼을 클릭했을 때 호출되는 함수
    public void clickCancel(View view) {
        // Do something in response to button
        parencount = 0;
        calcv.setText("");
        tv.setText("This is result");
    }
    //백 버튼을 클릭했을 때 호출되는 함수
    public void clickBack(View view) {
        // Do something in response to button
        CharSequence text = calcv.getText();
        int textSize = text.length();
        if(textSize == 0) return;

        if(text.charAt(textSize-1) == ' '){
            if(text.charAt(textSize-2) == '(') {
                text = text.subSequence(0,textSize-2);
                parencount--;
            }
            else text = text.subSequence(0,textSize-3);
        }else{
            if(text.charAt(textSize-1) == ')') {
                text = text.subSequence(0,textSize-2);
                parencount++;
            }
            else text = text.subSequence(0,textSize-1);
        }

        calcv.setText(text);
    }
    //등호 버튼을 클릭 했을 때 호출되는 함수
    public void clickResult(View view) {
        // Do something in response to button
        tv.setText(calculateFromJNI(calcv.getText().toString()));
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native String calculateFromJNI(String text);
}
